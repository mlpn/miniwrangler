package ro.mlpn.crisp.wrangler.csv;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import ro.mlpn.crisp.wrangler.exceptions.MiniWranglerException;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class CSVWriter {

    private CSVPrinter csvPrinter;

    public CSVWriter(OutputStream outputStream) {
        this.csvPrinter = preparePrinter(outputStream);
    }

    public void writeRow(final Iterable<?> values) {
        try {
            csvPrinter.printRecord(values);
        } catch (IOException e) {
            throw new MiniWranglerException("Exception occurred while writing csv row", e);
        }
    }

    public void handleEndOfInput() {
        try {
            csvPrinter.flush();
        } catch (IOException e) {
            throw new MiniWranglerException("Exception occurred while flushing", e);
        }
    }

    private CSVPrinter preparePrinter(OutputStream outputStream) {
        try {
            return new CSVPrinter(new OutputStreamWriter(outputStream), CSVFormat.DEFAULT);
        } catch (IOException e) {
            throw new MiniWranglerException("Exception occurred while preparing CSV printer", e);
        }
    }
}
