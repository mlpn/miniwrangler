package ro.mlpn.crisp.wrangler.transformation;

import lombok.AllArgsConstructor;
import org.apache.commons.csv.CSVRecord;
import ro.mlpn.crisp.wrangler.csv.CSVReader;
import ro.mlpn.crisp.wrangler.csv.CSVWriter;
import ro.mlpn.crisp.wrangler.dsl.Configuration;
import ro.mlpn.crisp.wrangler.exceptions.MiniWranglerException;
import ro.mlpn.crisp.wrangler.exceptions.MiniWranglerRowTransformException;
import ro.mlpn.crisp.wrangler.exceptions.RowTransformExceptionHandler;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@AllArgsConstructor
public class Transformer {

    private CSVReader reader;

    private CSVWriter writer;

    private Configuration configuration;

    private RowTransformExceptionHandler rowTransformExceptionHandler;

    private FunctionExecutor functionExecutor;

    public void transform() {
        assertInputHeadersMatchDefinition();

        writeOutputHeaders();
        reader.forEachRecord(this::transformRow);
        writer.handleEndOfInput();
    }

    private void assertInputHeadersMatchDefinition() {
        if (!configuration.getInputHeaderNames().equals(reader.getHeaderNames())) {
            throw new MiniWranglerException("Input column definition does not match actual file");
        }
    }

    private void transformRow(CSVRecord inputRecord) {
        try {
            var rowContext = new RowContext();

            addInputDataToContext(inputRecord, rowContext);
            addCalculatedVariablesToContext(rowContext);

            writeOutputValues(rowContext);
        } catch (Exception e) {
            handleRowException(e, inputRecord);
        }
    }

    private void handleRowException(Exception e, CSVRecord inputRecord) {
        var rowValues = StreamSupport
                .stream(inputRecord.spliterator(), false)
                .collect(Collectors.toList());
        var rowException = new MiniWranglerRowTransformException(e, rowValues);
        rowTransformExceptionHandler.handleRowException(rowException);
    }

    private void addCalculatedVariablesToContext(RowContext rowContext) {
        configuration.getCalculatedVariables().forEach((variableName, variableDefinition) -> {
            var variableValue = functionExecutor.getResult(variableDefinition, rowContext);
            rowContext.add(variableName, variableValue);
        });
    }

    private void addInputDataToContext(CSVRecord inputRecord, RowContext rowContext) {
        configuration.getInputColumns().forEach((variableName, sourceColumnName) -> {
            var value = inputRecord.get(sourceColumnName);

            rowContext.add(variableName, value);
        });
    }

    private void writeOutputValues(RowContext context) {
        var outputValues = configuration.getOutputColumns().values().stream()
                .map(context::getValueUsingColumnDefinition)
                .collect(Collectors.toList());
        writer.writeRow(outputValues);
    }

    private void writeOutputHeaders() {
        var outputHeaders = configuration.getOutputColumns().keySet();
        writer.writeRow(outputHeaders);
    }
}
