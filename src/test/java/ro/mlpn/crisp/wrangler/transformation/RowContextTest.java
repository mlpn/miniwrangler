package ro.mlpn.crisp.wrangler.transformation;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RowContextTest {

    @Test
    public void addingSameVariableTwice_willThrowException() {
        RowContext rowContext = new RowContext();
        rowContext.add("test", 1);
        rowContext.add("test2", "s");

        assertThatThrownBy(() -> rowContext.add("test", 2))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Variable test is already defined");
    }

    @Test
    public void whenColumnDefinitionNotVariable_columnDefinitionIsReturnedAsValue() {
        RowContext rowContext = new RowContext();
        rowContext.add("test", 1);

        assertThat(rowContext.getValueUsingColumnDefinition("ab")).isEqualTo("ab");
        assertThat(rowContext.getValueUsingColumnDefinition("test")).isEqualTo("test");
        assertThat(rowContext.getValueUsingColumnDefinition("{test")).isEqualTo("{test");
        assertThat(rowContext.getValueUsingColumnDefinition("test}")).isEqualTo("test}");
    }

    @Test
    public void whenColumnDefinitionIsVariableAndVariableStored_variableValueIsReturned() {
        RowContext rowContext = new RowContext();
        rowContext.add("test", 1);

        assertThat(rowContext.getValueUsingColumnDefinition("{test}")).isEqualTo(1);
    }

    @Test
    public void whenColumnDefinitionIsVariableAndVariableNotStored_exceptionIsThrown() {
        RowContext rowContext = new RowContext();

        assertThatThrownBy(() -> rowContext.getValueUsingColumnDefinition("{test}"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Variable test was not defined");
    }

    @Test
    public void rowContext_correctlyIdentifierVariables() {
        RowContext rowContext = new RowContext();

        assertTrue(rowContext.isVariableDefinition("{test}"));
        assertTrue(rowContext.isVariableDefinition("{te_st}"));
        assertFalse(rowContext.isVariableDefinition("{test"));
        assertFalse(rowContext.isVariableDefinition("test}"));
        assertFalse(rowContext.isVariableDefinition("{te st}"));
        assertFalse(rowContext.isVariableDefinition("{te_st} "));
    }
}