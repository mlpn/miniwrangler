package ro.mlpn.crisp.wrangler.transformation;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import ro.mlpn.crisp.wrangler.transformation.FunctionCallParser.Result;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;


public class FunctionCallParserTest {

    private FunctionCallParser functionCallParser = new FunctionCallParser();

    @Test
    public void whenVariableDefinitionIsCorrect_MethodIsExtracted() {
        assertThat(functionCallParser.getParsingResult("isInt({a})")).isEqualTo(new Result("isInt", of("{a}")));
        assertThat(functionCallParser.getParsingResult("properCase(test)")).isEqualTo(new Result("properCase", of("test")));
        assertThat(functionCallParser.getParsingResult("concat(abc, {te})")).isEqualTo(new Result("concat", of("abc", "{te}")));
    }

    @Test
    public void commasCanBeEscaped() {
        assertThat(functionCallParser.getParsingResult("concat('ab,c', test, '{t,e}')")).isEqualTo(new Result("concat", of("ab,c", "test", "{t,e}")));
    }

    @Test
    public void invalidMethodCallSignature_shouldThrowException() {
        Assertions.assertThatThrownBy(() -> functionCallParser.getParsingResult("abc({param1}, param2")) //method signature missing closing )
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Invalid method call abc({param1}, param2");
    }
}