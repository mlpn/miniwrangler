package ro.mlpn.crisp.wrangler.transformation;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static ro.mlpn.crisp.wrangler.transformation.FunctionDefinitions.removeFirstAndLastChar;

class RowContext {

    private static final Pattern VARIABLE_PATTERN = Pattern.compile("^[{](\\w*)[}]$");

    private Map<String, Object> valuesByVariableName = new HashMap<>();

    public void add(String variable, Object value) {
        if (valuesByVariableName.containsKey(variable)) {
            throw new IllegalArgumentException("Variable " + variable + " is already defined");
        }
        valuesByVariableName.put(variable, value);
    }

    public Object getValueUsingColumnDefinition(String outputColumnDefinition) {
        if (isVariableDefinition(outputColumnDefinition)) {
            var variableName = removeFirstAndLastChar(outputColumnDefinition);
            var returnValue = valuesByVariableName.get(variableName);
            if (returnValue == null) {
                throw new IllegalArgumentException("Variable " + variableName + " was not defined");
            }
            return returnValue;

        }
        return outputColumnDefinition;
    }

    boolean isVariableDefinition(String outputDefinition) {
        return VARIABLE_PATTERN.matcher(outputDefinition).matches();
    }
}
