package ro.mlpn.crisp.wrangler;

import ro.mlpn.crisp.wrangler.csv.CSVReader;
import ro.mlpn.crisp.wrangler.csv.CSVWriter;
import ro.mlpn.crisp.wrangler.dsl.Configuration;
import ro.mlpn.crisp.wrangler.dsl.ConfigurationProvider;
import ro.mlpn.crisp.wrangler.exceptions.RowTransformExceptionHandler;
import ro.mlpn.crisp.wrangler.transformation.FunctionExecutor;
import ro.mlpn.crisp.wrangler.transformation.FunctionCallParser;
import ro.mlpn.crisp.wrangler.transformation.Transformer;

import java.io.InputStream;
import java.io.OutputStream;


public class MiniWrangler {

    private Configuration configuration;

    private RowTransformExceptionHandler rowTransformExceptionHandler;

    public MiniWrangler(InputStream jsonConfigInputStream) {
        this(new ConfigurationProvider(jsonConfigInputStream).getConfiguration(), null);
    }

    public MiniWrangler(InputStream jsonConfigInputStream, RowTransformExceptionHandler rowTransformExceptionHandler) {
        this(new ConfigurationProvider(jsonConfigInputStream).getConfiguration(), rowTransformExceptionHandler);
    }

    public MiniWrangler(Configuration configuration, RowTransformExceptionHandler rowTransformExceptionHandler) {
        this.configuration = configuration;
        this.rowTransformExceptionHandler = rowTransformExceptionHandler != null ? rowTransformExceptionHandler : RowTransformExceptionHandler.THROWING_HANDLER;
    }

    public void transform(InputStream source, OutputStream destination) {
        var reader = new CSVReader(source);
        var writer = new CSVWriter(destination);
        var functionExecutor = new FunctionExecutor(new FunctionCallParser());

        Transformer transformer = new Transformer(reader, writer, configuration, rowTransformExceptionHandler, functionExecutor);
        transformer.transform();
    }
}
