package ro.mlpn.crisp.wrangler.exceptions;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class MiniWranglerRowTransformExceptionTest {

    @Test
    public void exceptionMessageShouldContainOriginalEntries() {
        var exception = new MiniWranglerRowTransformException(new RuntimeException("Bad color"), List.of("a", "b"));
        assertThat(exception).hasMessage("Exception occurred while transforming row. Reason : Bad color. Original input [a, b]");
    }
}