package ro.mlpn.crisp.wrangler.transformation;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class FunctionExecutorTest {

    @Test
    public void tryIngToExecuteAnUnknownMethod_willThrowException() {
        var methodCallParser = Mockito.mock(FunctionCallParser.class);
        var mockedResult = new FunctionCallParser.Result("transformTo42", List.of("param1", "param2"));
        when(methodCallParser.getParsingResult(any())).thenReturn(mockedResult);

        assertThatThrownBy(() -> new FunctionExecutor(methodCallParser).getResult(null, null))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Unknown method transformTo42");
    }
}