package ro.mlpn.crisp.wrangler.transformation;

import java.lang.reflect.Method;
import java.util.List;

public class FunctionExecutor {

    private FunctionCallParser functionCallParser;

    private Method[] availableMethods; //no need to read them every time a function is called

    public FunctionExecutor(FunctionCallParser functionCallParser) {
        this.functionCallParser = functionCallParser;
        this.availableMethods = getAvailableMethods();
    }

    public Object getResult(String variableDefinition, RowContext rowContext) {
        var methodCallParsingResult = functionCallParser.getParsingResult(variableDefinition);
        var method = getMethodToExecute(methodCallParsingResult.getMethodName());
        var invocationArguments = getInvocationArguments(methodCallParsingResult.getParameters(), rowContext);
        try {
            return method.invoke(null, invocationArguments);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Object[] getInvocationArguments(List<String> argumentsDefinition, RowContext rowContext) {
        return argumentsDefinition.stream()
                .map(rowContext::getValueUsingColumnDefinition)
                .toArray(Object[]::new);
    }

    private Method[] getAvailableMethods() {
        return FunctionDefinitions.class.getDeclaredMethods();
    }

    private Method getMethodToExecute(String methodName) {
        for (var method : availableMethods) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }
        throw new IllegalArgumentException("Unknown method " + methodName);
    }
}
