package ro.mlpn.crisp.wrangler.transformation;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static ro.mlpn.crisp.wrangler.transformation.FunctionDefinitions.*;


public class FunctionDefinitionsTest {

    @Test
    public void toInt_shouldWork() {
        assertThat(toInt("2")).isEqualTo(2);
    }

    @Test
    public void toDecimal_shouldWork() throws ParseException {
        assertThat(toDecimal("2,242.34", "#,##0.0#")).isEqualByComparingTo(new BigDecimal("2242.34"));
    }

    @Test
    public void dateFunctions_shouldWork() {
        var epochDay = LocalDate.ofEpochDay(0); //1970-01-01

        assertThat(date(epochDay.getYear(), epochDay.getMonthValue(), epochDay.getDayOfMonth())).isEqualTo(epochDay);
        assertThat(formatDate(epochDay, "yyyy-MM-dd")).isEqualTo("1970-01-01");
    }

    @Test
    public void removeFirstAndLastChar_shouldWorkAsExpected() {
        assertThatThrownBy(() -> removeFirstAndLastChar("sv"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("String sv must have length > 2 in order to remove first and last char");

        assertThat(removeFirstAndLastChar("sva")).isEqualTo("v");
    }

    @Test
    public void properCase_shouldWork() {
        assertThat(properCase("red DraGon")).isEqualTo("Red Dragon");
        assertThat(properCase("BlaCk foX")).isEqualTo("Black Fox");
        assertThat(properCase("bLue_cat")).isEqualTo("Blue_cat");
    }
}