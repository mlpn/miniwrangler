package ro.mlpn.crisp.wrangler.exceptions;

public class MiniWranglerException extends RuntimeException {

    public MiniWranglerException(String message, Throwable e) {
        super(message, e);
    }

    public MiniWranglerException(String message) {
        super(message);
    }
}
