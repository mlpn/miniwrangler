package ro.mlpn.crisp.wrangler.csv;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import ro.mlpn.crisp.wrangler.exceptions.MiniWranglerException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.function.Consumer;

public class CSVReader {

    private CSVParser parser;

    public CSVReader(InputStream csvFile) {
        this.parser = prepareParser(csvFile);
    }

    public void forEachRecord(Consumer<CSVRecord> csvRecordConsumer) {
        for (var csvRecord : parser) {
            csvRecordConsumer.accept(csvRecord);
        }
    }

    private CSVParser prepareParser(InputStream csvFile) {
        try {
            return CSVFormat.DEFAULT
                    .withHeader()
                    .withFirstRecordAsHeader()
                    .parse(new InputStreamReader(csvFile));
        } catch (Exception e) {
            throw new MiniWranglerException("Could not read input csv file", e);
        }
    }

    public List<String> getHeaderNames() {
        return parser.getHeaderNames();
    }
}
