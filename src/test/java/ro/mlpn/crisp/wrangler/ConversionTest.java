package ro.mlpn.crisp.wrangler;

import org.junit.Test;
import ro.mlpn.crisp.wrangler.exceptions.MiniWranglerRowTransformException;
import ro.mlpn.crisp.wrangler.exceptions.RowTransformExceptionHandler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertArrayEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ConversionTest {

    @Test
    public void correctSampleConversion_shouldTransformOk() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        MiniWrangler miniWrangler = new MiniWrangler(ConversionTest.class.getResourceAsStream("/config.json"));
        miniWrangler.transform(ConversionTest.class.getResourceAsStream("/correct-input.csv"), outputStream);

        var baselineFile = ConversionTest.class.getResourceAsStream("/correct-output.csv").readAllBytes();
        assertArrayEquals(baselineFile, outputStream.toByteArray());
    }

    @Test
    public void incorrectFileNoErrorHandler_willThrownException() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        MiniWrangler miniWrangler = new MiniWrangler(ConversionTest.class.getResourceAsStream("/config.json"));
        assertThatThrownBy(() -> miniWrangler.transform(ConversionTest.class.getResourceAsStream("/correct-input-with-extra-wrong-line.csv"), outputStream))
                .isInstanceOf(MiniWranglerRowTransformException.class)
                .hasMessageStartingWith("Exception occurred while transforming row");
    }

    @Test
    public void incorrectFileWithPermissiveHandler_shouldTransformOk() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        RowTransformExceptionHandler exceptionHandler = mock(RowTransformExceptionHandler.class);

        MiniWrangler miniWrangler = new MiniWrangler(ConversionTest.class.getResourceAsStream("/config.json"), exceptionHandler);
        miniWrangler.transform(ConversionTest.class.getResourceAsStream("/correct-input-with-extra-wrong-line.csv"), outputStream);

        var baselineFile = ConversionTest.class.getResourceAsStream("/correct-output.csv").readAllBytes();
        assertArrayEquals(baselineFile, outputStream.toByteArray());

        verify(exceptionHandler, times(1)).handleRowException(any());
    }
}
