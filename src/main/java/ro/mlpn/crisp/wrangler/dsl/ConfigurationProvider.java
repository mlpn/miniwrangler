package ro.mlpn.crisp.wrangler.dsl;

import com.google.gson.Gson;
import ro.mlpn.crisp.wrangler.exceptions.MiniWranglerException;

import java.io.InputStream;
import java.io.InputStreamReader;

public class ConfigurationProvider {

    private InputStream inputStream;

    public ConfigurationProvider(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public Configuration getConfiguration() {
        try {
            var gson = new Gson();
            return gson.fromJson(new InputStreamReader(inputStream), Configuration.class);
        } catch (RuntimeException e) {
            throw new MiniWranglerException("Exception occurred while loading configuration", e);
        }
    }
}
