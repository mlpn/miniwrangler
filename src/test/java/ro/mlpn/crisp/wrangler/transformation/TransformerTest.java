package ro.mlpn.crisp.wrangler.transformation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ro.mlpn.crisp.wrangler.csv.CSVReader;
import ro.mlpn.crisp.wrangler.csv.CSVWriter;
import ro.mlpn.crisp.wrangler.dsl.Configuration;
import ro.mlpn.crisp.wrangler.exceptions.MiniWranglerException;
import ro.mlpn.crisp.wrangler.exceptions.RowTransformExceptionHandler;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransformerTest {
    @Mock
    private CSVReader reader;
    @Mock
    private CSVWriter writer;
    @Mock
    private Configuration configuration;
    @Mock
    private RowTransformExceptionHandler rowTransformExceptionHandler;
    @Mock
    private FunctionExecutor functionExecutor;

    private Transformer transformer;

    @Before
    public void setUp() throws Exception {
        transformer = new Transformer(reader, writer, configuration, rowTransformExceptionHandler, functionExecutor);
    }

    @Test
    public void inCaseOfHeaderMissMatch_exceptionIsThrown() {
        when(reader.getHeaderNames()).thenReturn(List.of("a", "b"));
        when(configuration.getInputHeaderNames()).thenReturn(List.of("a", "b", "c"));

        assertThatThrownBy(() -> transformer.transform())
                .isInstanceOf(MiniWranglerException.class)
                .hasMessageStartingWith("Input column definition does not match actual file");
    }
}