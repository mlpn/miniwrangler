package ro.mlpn.crisp.wrangler.exceptions;

import lombok.Getter;

import java.util.List;

public class MiniWranglerRowTransformException extends MiniWranglerException {

    @Getter
    private List<String> rowValues;

    public MiniWranglerRowTransformException(Throwable e, List<String> rowValues) {
        super("Exception occurred while transforming row. Reason : " + e.getMessage() + ". Original input " + rowValues, e);
        this.rowValues = rowValues;
    }
}
