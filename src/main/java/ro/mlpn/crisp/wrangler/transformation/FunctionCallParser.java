package ro.mlpn.crisp.wrangler.transformation;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static ro.mlpn.crisp.wrangler.transformation.FunctionDefinitions.removeFirstAndLastChar;

public class FunctionCallParser {

    private static final Pattern METHOD_PATTERN = Pattern.compile("^(\\w*)[(](.*)[)]$");
    private static final String NOT_ESCAPED_COMMAS = ",(?=([^\']*\'[^\']*\')*[^\']*$)";

    public Result getParsingResult(String methodVariableDefinition) {
        var matcher = METHOD_PATTERN.matcher(methodVariableDefinition);
        if (matcher.find()) {
            return new Result(matcher.group(1), splitParams(matcher.group(2)));
        }
        throw new IllegalArgumentException("Invalid method call " + methodVariableDefinition);
    }

    private List<String> splitParams(String parameters) {
        //based on https://stackabuse.com/regex-splitting-by-character-unless-in-quotes/
        return Arrays.stream(parameters.split(NOT_ESCAPED_COMMAS))
                .map(String::trim)
                .map(this::removeEscapeCharsIfRequired)
                .collect(Collectors.toList());
    }

    private String removeEscapeCharsIfRequired(String original) {
        if (original.startsWith("'") && original.endsWith("'")) {
            return removeFirstAndLastChar(original);
        }
        return original;
    }

    @Data
    @AllArgsConstructor
    public static class Result {

        private String methodName;

        private List<String> parameters;
    }
}
