package ro.mlpn.crisp.wrangler.exceptions;

public interface RowTransformExceptionHandler {

    RowTransformExceptionHandler LOGGING_HANDLER = exception -> {
        System.out.println("Row exception (" + exception.getCause().getMessage() + "). Row values (" + exception.getRowValues() + "). Processing will continue");
    };

    RowTransformExceptionHandler THROWING_HANDLER = exception -> {
        throw exception;
    };

    void handleRowException(MiniWranglerRowTransformException exception);
}
