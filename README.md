# Mini-Wrangler
Java library used to transform CSV files

## Prerequisites
In order to build the project `Java 11` and `Maven` is required

## Dependencies
For version `1.0` the only compile time dependencies are :
*  `org.apache.commons:commons-csv:jar:1.7` used to read/write CSV
*  `com.google.code.gson:gson:jar:2.8.5:compile` use to parse the transform config file

Other dependencies:
* `org.projectlombok:lombok:jar:1.18.8` to avoid the boilerplate code a bit
* `junit:junit:jar:4.12` / `org.mockito:mockito-core:jar:2.28.2` / `org.assertj:assertj-core:jar:3.13.2` for testing

## Usage
Instantiating the MiniWrangler and triggering the transform using external DSL:
```java
try (var config = ...; var inputStream = ...; var outputStream = ...) {
    var miniwrangler = new MiniWrangler(config, RowTransformExceptionHandler.LOGGING_HANDLER);
    miniwrangler.transform(inputStream, outputStream);
}
```
In case the Configuration is known upfront a constructor exists to support this way o working:
```java
var configuration = new Configuration();
//prepare configuration object
try (var inputStream = ...; var outputStream = ...) {
    var miniwrangler = new MiniWrangler(config, RowTransformExceptionHandler.LOGGING_HANDLER);
    miniwrangler.transform(inputStream, outputStream);
}
```
A few things to consider about the usage:
* considering this is a library it will not close the streams and it's the user decision how/when to do that
* once a `MiniWrangler` instance is created it can be used by multiple threads (it is thread-safe)
* besides the configuration the library expects an implementation of `RowTransformExceptionHandler` that will be called for every row that encounters an exception during transformation. 
  There are two pre-built implementations `LOGGING_HANDLER` that outputs to console and `THROWING_HANDLER` (default if not provided) that will bubble the exception

To view some examples on usage one can check the `ConversionTest` class.

## High-level overview of transformation
Before starting the row transformation two steps are taken :
* validate input structure
* write output headers row

Transformation steps (loop over input rows):
* load input column values in variables 
* compute new variables using the input variables and available functions
* write output row using the output definition and available variables

## DSL
Sample:
```json
{
  "inputColumns": {
    "order_nr": "Order Number",
    "year": "Year",
    "month": "Month",
    "day": "Day",
    "product_nr": "Product Number",
    "product_name": "Product Name",
    "count": "Count",
    "extra_1": "Extra Col1",
    "extra_2": "Extra Col2",
    "empty": "Empty Column"
  },
  "calculatedVariables": {
    "order_id_int": "toInt({order_nr})",
    "year_int": "toInt({year})",
    "month_int": "toInt({month})",
    "day_int": "toInt({day})",
    "date": "date({year_int}, {month_int}, {day_int})",
    "count_decimal": "toDecimal({count}, '#,##0.0#')",
    "formatted_date": "formatDate({date}, yyyy-MM-dd)",
    "cased_product_name": "properCase({product_name})"
  },
  "outputColumns": {
    "OrderId": "{order_id_int}",
    "OrderDate": "{formatted_date}",
    "ProductId": "{product_nr}",
    "ProductName": "{cased_product_name}",
    "Quantity": "{count_decimal}",
    "Unit": "kg"
  }
}
```
Considerations :
* in `inputColumns` the user defines the variables using the source column names
* new variables can be calculated using the `calculatedVariables` definitions. Functions operate either with values from previous variables ex : `toInt({month})` or with raw values ex : `formatDate({date}, yyyy-MM-dd)`
* commas from raw values can be escaped using `'` (ex : `'#,##0.0#'`)
* output columns are defined in the following structure `"Column name": "column value definition"`
* the output column definition can be either a previous defined variable or a fixed value (ex: `Unit` column from sample)

## Implementation details
The most interesting part of the library is clearly the `transformation` package
* `Transformer.java` - the class that orchestrates the transformation
* `RowContext.java` - a wrapper over the `variable store`  which is a map
* `FunctionDefinitions.java` - it's the class where all the methods available for `computed variables` are defined. 
In order to extend the available functions the only thing needed is to define them in this class.
* `FunctionCallParser.java` - class that brakes a function call definition (ex: `add({int_1}, {int_2})`) in function name (`add`) and list of arguments (`[{int_1}, {int_2}]`)
* `FunctionExecutor.java` - uses input from `FunctionCallParser` to execute method from `FunctionDefinitions` using reflection.

#### Assumptions / simplifications
* method overloading and nested methods are not possible on `calculatedVariables` definition
* it was assumed the input csv file has a header row. DSL changes can be made so input assigment is done using position
* Sometimes libraries are available to fulfill a particular requirement (ex apache libraries for `properCase` function) 
but i opted to keep the dependencies as small as possible and provide own implementation

#### Future improvements
* allow method nesting. This is probably the one that will make the DSL as nice as possible
* it would be nice to support expressions like : `if / else` . With time and new functionality the DSL will be hard to maintain 
with the current implementation and it will probably be best to migrate to something based on `ANTLR`
* at this point the `Transformer` class is clearly targeting a CSV transformation (works with CSV reader/writer and also with CSVRecord from apache) but it would be nice to make it 
agnostic. By providing two interfaces `Reader` and `Writer` and by getting rid of `CSVRecord` this library could convert a lot more formats even cross format (ex: `XML` -> `CSV`)
* `CSVRecord` from apache is final and also hides the constructor thus hard to test with and this is another reason to make it an implementation detail in a `CSVReader`
* validate DSL before starting the transformation. In case of a typo or obvious error in definition the processing could be stopped early. 
Another good example of error that can be caught while reading the DSL definition is if the correct value types are send to a particular function. (ex: determine reading the DSL the `toInt` function is called with a `LocalDate` as param instead of a `String`)
* better error handling. In particular better error messages for case like row exceptions the underling exception cause should be exposed
* add `module-info.java` and make sure the library exposes only the required packages
 
