package ro.mlpn.crisp.wrangler.dsl;

import org.junit.Test;
import ro.mlpn.crisp.wrangler.exceptions.MiniWranglerException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ConfigurationProviderTest {

    @Test
    public void failingToReadStream_shouldThrowCorrectException() {
        assertThatThrownBy(() -> new ConfigurationProvider(null).getConfiguration())
                .isInstanceOf(MiniWranglerException.class)
                .hasMessage("Exception occurred while loading configuration");
    }

    @Test
    public void withCorrectInput_ProviderShouldConvertSuccessfully() {
        Configuration configuration = new ConfigurationProvider(ConfigurationProviderTest.class.getResourceAsStream("/config.json")).getConfiguration();
        assertThat(configuration.getInputColumns()).hasSize(10);
        assertThat(configuration.getOutputColumns()).hasSize(6);
    }
}