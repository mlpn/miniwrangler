package ro.mlpn.crisp.wrangler.transformation;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

class FunctionDefinitions {

    public static Integer toInt(String strValue) {
        return Integer.valueOf(strValue);
    }

    public static BigDecimal toDecimal(String strValue, String format) throws ParseException {
        var decimalFormat = new DecimalFormat(format);
        decimalFormat.setParseBigDecimal(true);
        return (BigDecimal) decimalFormat.parse(strValue);
    }

    public static LocalDate date(Integer year, Integer month, Integer day) {
        return LocalDate.of(year, month, day);
    }

    public static String formatDate(LocalDate localDate, String format) {
        var formatter = DateTimeFormatter.ofPattern(format);
        return formatter.format(localDate);
    }

    public static String removeFirstAndLastChar(String original) {
        if (original == null || original.length() < 3) {
            throw new IllegalArgumentException("String " + original + " must have length > 2 in order to remove first and last char");
        }
        return original.substring(1, original.length() - 1);
    }

    public static String properCase(String text) {
        if (text == null || text.isEmpty()) {
            return text;
        }

        var converted = new StringBuilder();

        boolean convertNext = true;
        for (char ch : text.toCharArray()) {
            if (Character.isSpaceChar(ch)) {
                convertNext = true;
            } else if (convertNext) {
                ch = Character.toTitleCase(ch);
                convertNext = false;
            } else {
                ch = Character.toLowerCase(ch);
            }
            converted.append(ch);
        }

        return converted.toString();
    }
}
