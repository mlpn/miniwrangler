package ro.mlpn.crisp.wrangler.dsl;

import lombok.Data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Data
public class Configuration {

    private LinkedHashMap<String, String> inputColumns = new LinkedHashMap<>();

    private LinkedHashMap<String, String> calculatedVariables = new LinkedHashMap<>();

    private LinkedHashMap<String, String> outputColumns = new LinkedHashMap<>();

    public List<String> getInputHeaderNames() {
        return new ArrayList<>(inputColumns.values());
    }
}
