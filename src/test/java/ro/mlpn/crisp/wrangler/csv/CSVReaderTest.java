package ro.mlpn.crisp.wrangler.csv;

import org.apache.commons.csv.CSVRecord;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import ro.mlpn.crisp.wrangler.exceptions.MiniWranglerException;

import java.util.List;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

public class CSVReaderTest {

    @Test
    public void providingInvalidFileStream_shouldThrowCorrectException() {
        assertThatThrownBy(() -> new CSVReader(null))
                .isInstanceOf(MiniWranglerException.class)
                .hasMessage("Could not read input csv file");
    }

    @Test
    public void withCorrectStream_shouldAllowHeaderRead() {
        CSVReader csvReader = new CSVReader(CSVReaderTest.class.getResourceAsStream("/correct-output.csv"));

        assertThat(csvReader.getHeaderNames()).isEqualTo(List.of("OrderId", "OrderDate", "ProductId", "ProductName", "Quantity", "Unit"));
    }

    @Test
    public void withCorrectStream_shouldReadAllRows() {
        CSVReader csvReader = new CSVReader(CSVReaderTest.class.getResourceAsStream("/correct-output.csv"));

        var mockedConsumer = (Consumer<CSVRecord>) mock(Consumer.class);
        csvReader.forEachRecord(mockedConsumer);

        Mockito.verify(mockedConsumer, times(2)).accept(any());

    }
}